﻿using System;
using System.Threading.Tasks;

namespace YolvaTestTask
{
    class Program
    {
        static void Main(string[] args)
        {
            string address = InputAddress();
            int frequency = InputFrequency();
            string fileName = InputFileName();

            GeoService geoService = new OSMGeoService();
            var loadDataTask = geoService.GetCoordinatesForAddressAsync(address, frequency);

            loadDataTask.Wait();

            var data = loadDataTask.Result;

            System.IO.File.WriteAllText($@"{fileName}.txt", string.Join("\n", data));
        }

        static string InputAddress()
        {
            Console.WriteLine("Введите адрес:");
            return Console.ReadLine();
        }

        static int InputFrequency()
        {
            Console.WriteLine("Введите частоту точек:");
            if (int.TryParse(Console.ReadLine(), out int value) && value > 0) 
            {
                return value;
            }
            else 
            {
                return 1;
            }
        }

        static string InputFileName()
        {
            Console.WriteLine("Введите имя файла:");
            return Console.ReadLine();
        }
    }
}
