﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace YolvaTestTask
{
    public class OSMGeoService : GeoService
    {
        protected override async Task<string> LoadAddressData(string address)
        {
            string url = $"https://nominatim.openstreetmap.org/search?q={address}&format=json&polygon_geojson=1&limit=1&email=eul8hw9yfkfd@mail.ru";
            
            return await new HttpClient().GetStringAsync(url);
        }

        protected override List<string> ExtractCoordinatePairs(string data, int frequency)
        {
            using JsonDocument doc = JsonDocument.Parse(data);
            if (doc.RootElement.GetArrayLength() == 0)
            {
                return new List<string>();
            }

            JsonElement firstResultCoordinates = doc.RootElement[0].GetProperty("geojson").GetProperty("coordinates");

            string pattern = @"(?<=\[)-?\d*\.\d*,-?\d*\.\d*(?=\])";
            var matchedCoordinatePairs = Regex.Matches(firstResultCoordinates.ToString(), pattern);

            return matchedCoordinatePairs.Select(match => match.Value).ToList();
        }
    }
}
