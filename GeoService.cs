﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace YolvaTestTask
{
    public abstract class GeoService
    {
        public async Task<List<string>> GetCoordinatesForAddressAsync(string address, int frequency)
        {
            if (frequency < 1)
            {
                throw new ArgumentOutOfRangeException("frequency", "Frequency should be 1 or greater.");
            }
            
            return ExtractCoordinatePairs(await LoadAddressData(address), frequency)
                .Where((pair, index) => index % frequency == 0)
                .ToList();
        }

        protected abstract Task<string> LoadAddressData(string address);
        protected abstract List<string> ExtractCoordinatePairs(string data, int frequency);
    }
}
